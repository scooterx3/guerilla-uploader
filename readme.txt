Guerilla Uploader

A bash script which injects a PHP upload script which deletes itself after the operation completes (on success or failure). 

Used for those situations when setting up an FTP or SSH account is just too much work for a single simple upload. 